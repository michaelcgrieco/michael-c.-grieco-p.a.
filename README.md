The defense attorneys at The Grieco Criminal Law Center are experienced, passionate and aggressive and have built a reputation for providing superior representation to all of our clients in Miami Dade County, Broward County, and throughout the State of Florida.

Address: 175 SW 7th Street, #2410, Miami, FL 33130

Phone: 305-857-0034
